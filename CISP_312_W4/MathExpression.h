#pragma once
#include "stdafx.h"
#include "OperatorToken.h"
#include "OperandToken.h"
#include <vector>
#include <array>

using namespace std;

class MathExpression
{
private:
	void ReadNumber(istream & in);
	void ReadOperator(istream & in);
	void ReadNamedObject(istream & in); // can be either a variable or a function
	vector<string> log;
	void Log(string message);
	vector<string> RPN;

	void AddRPN(int value);
	void AddRPN(float value);
	void AddRPN(char value);
	void AddRPN(string value);

public:
	const string operatorChars = "+-*/()^!%";
	const string operandChars = "0123456789.-+"; // Note: + and - are can be a sign for an operand, or an operator
	bool useDebugMode;

	MathExpression();
	MathExpression(std::string str);

	queue<MathToken> output;	// contains all types of tokens
	stack<MathToken> opStack;	// contains operators and functions

	void ProcessInput();
	void ProcessInput(string str);
	void ProcessInput(istream & in);

	void Move();			// empties the entire operator-stack into output-queue
	void Move(int count);	// moves a set amount of tokens from the stack to the queue

	string ToRPN();
	string getLog();
};

