#pragma once
#include "Token.h"
#include "FunctionToken.h"

class OperandToken : 
	public MathToken
{
private:

	// these are here to make intellisense easier to browse. They can easily be changed to public
	OperandToken operator +(const int value);
	OperandToken operator +(const float value);
	OperandToken operator -(const int value);
	OperandToken operator -(const float value);
	OperandToken operator *(const int value);
	OperandToken operator *(const float value);
	OperandToken operator /(const int value);
	OperandToken operator /(const float value);
	OperandToken operator %(const int value);
	OperandToken operator %(const float value);

public:
	OperandToken(int value);
	OperandToken(float value);
	OperandToken(TokenType type);

	// Conversion from MathToken by constructor
	OperandToken(const MathToken& token);

	/* operator overloading - this is done to simplify code elsewhere such as the Evaluator	*/    
	OperandToken operator +(const OperandToken &value);
	OperandToken operator -(const OperandToken &value);
	OperandToken operator *(const OperandToken &value);
	OperandToken operator /(const OperandToken &value);
	OperandToken operator !();
	OperandToken operator ^(const OperandToken &value);
	OperandToken operator %(const OperandToken &value);
    
	bool Reduce();		// try to change from float to int
};

