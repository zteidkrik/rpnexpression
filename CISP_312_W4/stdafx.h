// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <stdio.h>

#ifdef _WIN32
	#include "targetver.h"
	#include <tchar.h>
#endif

// TODO: reference additional headers your program requires here
#include <iostream>
#include <sstream>
#include <string>
#include <stack>
#include <queue>
#include <math.h>

#include "Evaluator.h"
#include "MathExpression.h"
#include "Token.h"
#include "OperandToken.h"
#include "OperatorToken.h"
#include "FunctionToken.h"