#pragma once
#include "stdafx.h"

using namespace std;

class FunctionToken :
	public MathToken
{
private:
	TokenFunction function;
	TokenArity arity;
	string _name;
public:
	const float PI = 3.14159265359f;
	FunctionToken(string name);

	// Conversion from MathToken by constructor
	FunctionToken(const MathToken& token);

	// Conversion from MathToken by type-cast
	//operator MathToken(){ return MathToken(); }
    
    MathToken operator<<(const MathToken &value);

	TokenArity GetArity();
	TokenFunction GetFunction();
	void SetFunction();

	string ToString();
};

