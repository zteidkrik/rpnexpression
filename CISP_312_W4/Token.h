#pragma once
#include "stdafx.h"

enum TokenType{ Null, Character, Integer, Float };
enum TokenCategory{ Operand, Operator, Function, Variable};
enum TokenOperation{ NoOperation, Negation, Addition, Subtraction, Multiplication, Division, Modulus, Power, Grouping, Factorial };
enum TokenAssociativity{ NoAssociativity, Left, Right };
enum TokenPrecedence{ NoPrecedence, GroupingScope, AdditionSubtraction, MultiplicationDivisionModulo, ExponentsRoots }; // These are intentionally in-order!
enum TokenArity{ Nullary, Unary, Binary };
enum TokenFunction{ Tangent, Cosine, Sine, SquareRoot };

union TokenValue{
	int iValue;
	float fValue;
	char cValue;
};

class MathToken{	

public:
	MathToken();
	MathToken(TokenCategory cat);

	TokenCategory category;
	TokenType type;
	TokenValue Value;

	void Convert();
	void Convert(TokenType t);

	virtual std::string TypeName();
	virtual std::string TypeCategory();
    virtual std::string ToString();
	virtual std::string ToString(std::string name);
};