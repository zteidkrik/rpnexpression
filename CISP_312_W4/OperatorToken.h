#pragma once
#include "Token.h"
class OperatorToken :
	public MathToken
{
private:
	TokenArity arity;
	TokenAssociativity associativity;
	TokenPrecedence precedence;
	TokenOperation op;

	void SetOperation();
	void SetAssociativity();
	void SetPrecedence();
public:
	OperatorToken(char value);

	// Conversion from MathToken
	OperatorToken(const MathToken& token);

	TokenArity GetArity();
	TokenOperation GetOperation();
	TokenAssociativity GetAssociativity();
	TokenPrecedence GetPrecedence();
    
    // relational operators
    bool operator<(OperatorToken & token);
    bool operator>(OperatorToken & token);

	std::string TypeName();
};

