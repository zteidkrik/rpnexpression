#pragma once
#include "MathExpression.h"
#include "OperandToken.h"
#include "OperatorToken.h"
#include "FunctionToken.h"

using namespace std;

class Evaluator
{
public:
	Evaluator();	// constructor
	Evaluator(bool useDebugMode);
	static bool UseDebugMode;

	OperandToken Evaluate();
	OperandToken Evaluate(string exp);
	OperandToken Evaluate(MathExpression exp);
	static void DisplayResultInfo();
	static void DisplayLog();
protected:
	static vector<string> log;
	void Log(string message);

	OperandToken Evaluate(OperandToken a, OperatorToken op);
	OperandToken Evaluate(OperandToken a, OperandToken b, OperatorToken op);
};

