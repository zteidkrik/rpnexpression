#include "stdafx.h"
#include "Token.h"

using namespace std;

MathToken::MathToken(){

}

MathToken::MathToken(TokenCategory cat){
	category = cat;
}

void MathToken::Convert(){
	if (type == Integer){
		Convert(Float);
	}
	else if (type == Float){
		Convert(Integer);
	}
	else{
		throw runtime_error("Current type can't be converted");
	}
}

void MathToken::Convert(TokenType t){
	// only allowed conversions are int<->float 

	if (type != Float && type != Integer){
		throw runtime_error("Current type can't be converted");
	}
	else if (t != Float && t != Integer){
		throw runtime_error("Invalid type to be converted to");
	}

	// can't be same type
	if (type != t){
		if (type == Float){
			// to int
			Value.iValue = static_cast<int>(Value.fValue);
		}
		else if (type == Integer){
			// to float
			Value.fValue = static_cast<float>(Value.iValue);
		}

		type = t;
	}
}

string MathToken::ToString(){
	stringstream ss;
	string colon = "";

	if (Evaluator::UseDebugMode){
		ss << "Token:" << TypeCategory() << ":" << TypeName();
		colon = ":";
	}

	if (type == Float){
		ss << colon << to_string(Value.fValue);
	}
	else if (type == Integer){
		ss << colon << to_string(Value.iValue);
	}
	else if (type == Character){
		ss << colon << Value.cValue;
	}

	return ss.str();
}

string MathToken::ToString(string info){
	stringstream ss;

	ss << "Type: " << TypeName() << endl
		<< "Integer Value: " << Value.iValue << endl
		<< "Float Value: " << Value.fValue << endl
		<< "Char Value: " << Value.cValue << endl
		<< "---------------------------------" << endl;

	return ss.str();
}

string MathToken::TypeCategory(){
	if (category == TokenCategory::Operand){
		return "Operand";
	}
	else if (category == Operator){
		return "Operator";
	}
	else if (category == Function){
		return "Function";
	}
	else if (category == Variable){
		return "Variable";
	}
}

string MathToken::TypeName(){
	if (type == Float){
		return "Float";
	}
	else if (type == Integer){
		return "Integer";
	}
	else if (type == Character){
		return "Character";
	}
	else if (type == Null){
		return "Null";
	}

	return "Unknown";
}