// CISP_312_W4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace std;

void Wait(string message){
	cout << message << endl;

	char c;
	while (cin.get(c)){
		if (c == '\n'){
			return;
		}
	}
}

void ClearInputBuffer(){
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

void DisplayOperations(){
    
#ifdef _WIN32
	system("cls");
#endif
    
#ifdef __APPLE__
    system("clear");
#endif

	cout << "Supported Operations:" <<	"\t\tUsage:"	<< "\tLimitations:"	<< endl
		<< "\tAddition"				<<	"\t\ta + b"		<< endl
		<< "\tSubtraction"			<<	"\t\ta - b"		<< endl
		<< "\tMultiplication"		<<	"\t\ta * b"		<< endl
		<< "\tDivision"				<<	"\t\ta / b"		<< "\tb != 0"		<< endl
		<< "\tModulus"				<<	"\t\t\ta % b"	<< endl
		<< "\tExponentiation"		<<	"\t\ta ^ b"		<< endl
		<< "\tFactorial"			<<	"\t\ta!"		<< "\ta > -1"		<< endl
		<< endl
		<< "Supported Functions:"   << endl
		<< "\tSine:"				<< "\t\t\tsin(a)"	<< "\tUses degrees"	<< endl
		<< "\tCosine:"				<< "\t\t\tcos(a)"	<< "\tUses degrees"	<< endl
		<< "\tTangent:"				<< "\t\ttan(a)"		<< "\tUses degrees"	<< endl
		<< "\tSquareRoot:"			<< "\t\tsqrt(a)"	<< "\ta > -1"		<< endl;

	return;
}
int DisplayMenu(){
	int input = 0;

	while (input < 1 || input > 5){
		system("cls");

		cout << "Awesome Calculator v1.0" << endl
			<< "---------------------------------------------" << endl
			<< "1. Evaluate your own expression" << endl
			<< "2. Evaluate a random expressions" << endl
			<< "3. View supported operations and functions" << endl
			<< "4. Toggle Debug Mode | Status: " << (Evaluator::UseDebugMode ? "En" : "Dis") << "abled\n"
			<< "5. Exit" << endl;
		

		if (!(cin >> input)){
			ClearInputBuffer();
		}
	}

	return input;
}

std::string getRandomOperator(){
	int value = rand() % 7;

	if (value == 0){
		return " +";
	}
	else if (value == 1){
		return " -";
	}
	else if (value == 2){
		return " *";
	}
	else if (value == 3){
		return " /";
	}
	else if (value == 4){
		return " ^"; 
	}
	else if (value == 5){
		return " %";
	}
	else if (value == 6){
		return "!";
	}

	return "";
}

std::string getRandonExpression(){
	int operandCount = rand() % 8 + 2;	// 2 to 10 operands
	int groupDepth = 0;					// can only go 2 parenthesis deep
	int maxExp = 10;	// max value allowed for the exponent
	float functBias = 0.25f;	// scalar to function ratio
	float groupBias = 0.30f; // probability of inserting '('
	bool nestedFunctions = false;

	int count = 0;

	stringstream ss;

	cout << "Creating Expression with " << operandCount << " operands" << endl;

	while (count < operandCount){

		// Randomly decide whether to add a parenthesis or not
		if (groupDepth < 2 && rand() < groupBias){
			ss << '(';
			groupDepth++;
		}

		ss << to_string((rand() % 100) - (rand() % 100));
		count++;

		if (groupDepth > 0 && rand() < groupBias){
			ss << ')';
			groupDepth--;
		}

		if (count < operandCount){
			ss << getRandomOperator() << " ";
		}
	}

	return ss.str();
}

int run()
{
	Evaluator eval = Evaluator();
	int input = 0;

	while (input != 5){
		input = DisplayMenu();

		try
		{
			if (input == 1){
				cout << eval.Evaluate().ToString() << endl;
				Evaluator::DisplayLog();
			}
			else if(input == 2){				

				std::string _exp = getRandonExpression();
				cout << eval.Evaluate(_exp).ToString() << endl;
				Evaluator::DisplayLog();
				ClearInputBuffer();

/*				Test(eval.Evaluate("+3"), 3);
				Test(eval.Evaluate("-3"), -3);
				Test(eval.Evaluate("3(*1"), 3);
				Test(eval.Evaluate("15 / 6"), 2.25f);
				Test(eval.Evaluate("-3 - 5"), -8);
				Test(eval.Evaluate("2 ^ 3"),  8.0f);
				Test(eval.Evaluate("2 ^ 2 ^ 2"), 16.0f);
				Test(eval.Evaluate("2.1 ^ 2.1 ^ 2.1"), 26.352501f); // 26.362530170624176585016323554347
				Test(eval.Evaluate("3 - -5"), 8);
                
                Test(eval.Evaluate("3()"), 3);  // parenthesis are dropped

				// These should all fail

				Test(eval.Evaluate("+3("), 3);  // no matching right parenthesis
                Test(eval.Evaluate("3(-)"),0);  // no number
				Test(eval.Evaluate("3(-"), 0);	// fails4*/
			}
			else if (input == 3){
				DisplayOperations();
				ClearInputBuffer();
			}
			else if (input == 4){
				Evaluator::UseDebugMode = !Evaluator::UseDebugMode;
				continue;
			}
			else if (input == 5){
				break;
			}

			// block thread for user to read result
			Wait("Press enter to continue");
		}
		catch (exception& e){
			cout << e.what() << endl;
			eval.DisplayLog();
			
			//ClearInputBuffer();
			// block thread for user to read result
			Wait("Press enter to resume");
			continue;		
		}
	}

    return 0;
}

#ifdef _WIN32
int _tmain(int argc, _TCHAR* argv[])
{
    return run();
}
#endif

#ifdef __APPLE__
int main(int argc, const char * argv[])
{
    return run();
}
#endif
    
