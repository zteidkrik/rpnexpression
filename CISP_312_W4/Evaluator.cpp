#include "stdafx.h"
#include "Evaluator.h"

using namespace std;

bool Evaluator::UseDebugMode;
vector<string> Evaluator::log;

Evaluator::Evaluator() : Evaluator(false){}

Evaluator::Evaluator(bool value){
	Evaluator::UseDebugMode = value;
}

OperandToken Evaluator::Evaluate(){
	return Evaluate(MathExpression());
}

OperandToken Evaluator::Evaluate(string str){
	return Evaluate(MathExpression(str));
}

OperandToken Evaluator::Evaluate(OperandToken a, OperatorToken op){
	if (op.GetOperation() == TokenOperation::Factorial){
		return !a; // ! is an overloaded operator
	}
	else{
		throw runtime_error("unsupported operation");
	}
}

OperandToken Evaluator::Evaluate(OperandToken a, OperandToken b, OperatorToken op){
	if (op.GetOperation() == Addition){
		return a + b;
	}
	else if (op.GetOperation() == Subtraction){
		return a - b;		
	}
	else if (op.GetOperation() == Multiplication){
		return a * b;
	}
	else if (op.GetOperation() == Division){
		return a / b;
	}
	else if (op.GetOperation() == Modulus){
		return a % b;
	}
	else if (op.GetOperation() == Power){
		return b ^ a; // flip b an a, since this is a right-associative operation
	}
	else{
		throw runtime_error("Operator not supported");
	}
}

OperandToken Evaluator::Evaluate(MathExpression exp){
	exp.useDebugMode = Evaluator::UseDebugMode;
	log.clear();

	stack<OperandToken> evalStack;
	MathToken token;		// token being read
	OperandToken result = OperandToken(Null);	// token being returned

	Log(exp.getLog());
	Log("---------------------------\nStarted Evaluating Expression");

	// Process output queue
	while (!exp.output.empty()){
		Log("\nStart of Iteration");
		token = exp.output.front();
		exp.output.pop();
        
		Log("Read from queue: " + token.ToString());

		if (token.category == Operand){
			OperandToken value = token;
			Log("Pushed to stack: " + value.ToString());
			evalStack.push(value);
			continue;
		}
		else if (token.category == Operator){
			OperatorToken op = token;
			OperandToken a = evalStack.top(), b = OperandToken(Null);
			Log("Popped from stack: " + a.ToString());
			evalStack.pop();

			if (op.GetArity() == Binary){
				b = evalStack.top();
				Log("Popped from stack: " + b.ToString());
				evalStack.pop();
				result = Evaluate(b, a, op);
			}
			else if (op.GetArity() == Unary){
				result = Evaluate(a, op);
			}

			Log("Ran Operation: " + op.ToString());
		}
		else if (token.category == Function){
			FunctionToken fn = token;
			//evalStack.pop();
			OperandToken a = evalStack.top();
			evalStack.pop();
			Log("Popped from stack: " + a.ToString());
            result = fn << a;
			Log("Invoked Function: " + fn.ToString());
		}

		Log("Pushed to stack: " + result.ToString());
		evalStack.push(result);
	}
    
    
	if (!evalStack.empty()){
		if (evalStack.size() > 1){
			cout << "Evaluation Error: Invalid Expression" << endl;
			evalStack.empty();
			result = OperandToken(0);
		}
		else{
			result = evalStack.top();
			evalStack.pop();
			Log("\nResult: " + result.ToString());
		}        
	}
	else{
		result = OperandToken(0); // nothing to process
	}

	Log("\nFinished Evaluating Expression\n---------------------------\n");

	return result;
}

void Evaluator::DisplayResultInfo(){
	// Display relevant info
	/*
	cout << "Type: " << last.TypeName() << endl
		<< "Integer Value: " << last.Value.iValue << endl
		<< "Float Value: " << last.Value.fValue << endl
		<< "Char Value: " << last.Value.cValue << endl
		<< "---------------------------------" << endl;*/
}

void Evaluator::Log(string message){
	if (Evaluator::UseDebugMode){
		log.push_back(message);
	}
}

void Evaluator::DisplayLog(){
	for (size_t t = 0; t < log.size(); t++){
		cout << log.at(t) << endl;
	}
}