#include "stdafx.h"
#include "OperandToken.h"
#include <math.h>

OperandToken::OperandToken(int value)
	: MathToken(Operand)
{
	Value.iValue = value;
	type = Integer;
}

OperandToken::OperandToken(float value)
	: MathToken(Operand)
{
	Value.fValue = value;
	type = Float;
}

OperandToken::OperandToken(TokenType datatype)
	: MathToken(Operand)
{
	type = datatype;

	if (type == Float){
		Value.fValue = 0.0f;
	}
	else if (type == Integer){
		Value.iValue = 0;
	}else{
        Value.cValue = 0;
    }
}

OperandToken::OperandToken(const MathToken& token)
	: MathToken(Operand)
{
	type = token.type;
	Value = token.Value;
}

/* operator overloading */

OperandToken OperandToken::operator+(const OperandToken& value){
	if (value.type == Integer){
		return operator+(value.Value.iValue);
	}
	else if (value.type == Float){
		return operator+(value.Value.fValue);
	}
	else{
		throw runtime_error("Unable to calculate using nsupported types");
	}
}

OperandToken OperandToken::operator+(const int value){
	OperandToken token = OperandToken(type);

	if (type == Integer){
		token.Value.iValue = Value.iValue + value;
	}
	else if (type == Float){
		token.Value.fValue = Value.fValue + static_cast<float>(value);
	}
	else{
		throw;
	}

	return token;
}

OperandToken OperandToken::operator+(const float value){
	OperandToken token = OperandToken(type);

	if (type == Float){
		token.Value.fValue = Value.fValue + value;
	}
	else if (type == Integer){
		token.Value.fValue = static_cast<float>(Value.iValue) + value;
	}
	else{
		throw;
	}

	return token;
}

OperandToken OperandToken::operator-(const OperandToken& value){
	if (value.type == Integer){
		return operator-(value.Value.iValue);
	}
	else if (value.type == Float){
		return operator-(value.Value.fValue);
	}
	else{
		throw;
	}
}

OperandToken OperandToken::operator-(const int value){
	OperandToken token = OperandToken(type);

	if (type == Float){
		token.Value.fValue = Value.fValue - static_cast<float>(value);
	}
	else if (type == Integer){
		token.Value.iValue = Value.iValue - value;
	}
	else{
		throw;
	}

	return token;
}

OperandToken OperandToken::operator-(const float value){
	OperandToken token = OperandToken(TokenType::Float);

	if (type == Float){
		token.Value.fValue = Value.fValue - value;
	}
	else if (type == Integer){
		token.Value.fValue = static_cast<float>(Value.iValue) - value;
	}
	else{
		throw;
	}

	return token;
}

OperandToken OperandToken::operator*(const OperandToken& value){
	if (value.type == Integer){
		return operator*(value.Value.iValue);
	}
	else if (value.type == Float){
		return operator*(value.Value.fValue);
	}
	else{
		throw;
	}
}

OperandToken OperandToken::operator*(const int value){
	OperandToken token = OperandToken(type);

	if (type == Float){
		token.Value.fValue = Value.fValue * value;
	}
	else if (type == Integer){
		token.Value.iValue = Value.iValue * value;
	}
	else{
		throw;
	}
	return token;
}

OperandToken OperandToken::operator*(const float value){
	OperandToken token = OperandToken(TokenType::Float);

	if (type == Float){
		token.Value.fValue = Value.fValue * value;
	}
	else if (type == Integer){
		token.Value.fValue = static_cast<float>(Value.iValue) * value;
	}
	else{
		throw;
	}
	return token;
}

OperandToken OperandToken::operator/(const OperandToken& value){
	// special cases
	if (value.Value.fValue == 0.0f || value.Value.iValue == 0){
		if (Value.fValue == 0.0f || Value.iValue == 0){
			throw runtime_error("Evaluation Error: Indeterminate");
		}
		else{
			throw runtime_error("Evaluation Error: Division by zero");
		}
	}

	// both values should be floats to avoid losing precision
	OperandToken token = value;

	if (token.type == Integer){
		token.Convert(Float);
	}

	if (type == Integer){
		Convert(Float);
	}

	token.Value.fValue = Value.fValue / token.Value.fValue;

	return token;
}

OperandToken OperandToken::operator%(const OperandToken& value){
	if (value.type == Integer){
		return operator%(value.Value.iValue);
	}
	else if (value.type == Float){
		return operator%(value.Value.fValue);
	}
	else{
		throw;
	}
}

OperandToken OperandToken::operator%(const int value){
	OperandToken token = OperandToken(type);

	if (type == Float){
		token.Value.fValue = fmodf(Value.fValue, static_cast<float>(value));
	}
	else if (type == Integer){
		token.Value.iValue = Value.iValue % value;
	}
	else{
		throw;
	}

	return token;
}

OperandToken OperandToken::operator%(const float value){
	OperandToken token = OperandToken(TokenType::Float);

	if (type == Float){
		token.Value.fValue = fmodf(Value.fValue, value);
	}
	else if (type == Integer){
		token.Value.fValue = fmodf(static_cast<float>(Value.iValue), value);
	}
	else{
		throw;
	}

	return token;
}

OperandToken OperandToken::operator^(const OperandToken& value){
	// pow doesn't return int types...so both tokens will be changed to floats to get a float-value from it
	Convert(Float);

    float f;
    if (value.type == Float){
        f = pow(Value.fValue, value.Value.fValue);
        
		if (isinf(f)){
			throw runtime_error("Exponentiation Overflow");
		}

		return OperandToken(f);
	}
	else if (value.type == Integer){
        f =pow(Value.fValue, static_cast<float>(value.Value.iValue));

		if (isinf(f)){
			throw runtime_error("Exponentiation Overflow");
		}

		return OperandToken(f);
	}
	else{
		throw runtime_error("Unsupported type");
	}	
}

OperandToken OperandToken::operator!(){
	OperandToken token = OperandToken(Integer); // Factorial only allows integers. The gamma function is suggested for decimals/floats
	token.Value.iValue = 1; // default value

	Convert(Integer);
	int count = Value.iValue;

	if (count > 16){
		throw runtime_error("Overflow");
	}else if (count > 0){
		for (int i = 1; i <= count; i++){
			token.Value.iValue *= i;
		}
	}
	else if (count < 0){ // 0! = 1
		throw range_error("Factorial value must be non-negative");
	}

	return token;
}

bool OperandToken::Reduce(){
	bool passed = false;

	if (type == Float && floor(Value.fValue) == Value.fValue){
		try{
			Convert(Integer);
			passed = true;
		}
		catch (...){} // silenty catch this
	}

	return passed;
}