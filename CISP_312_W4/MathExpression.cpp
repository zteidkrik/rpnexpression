#include "stdafx.h"
#include "MathExpression.h"

MathExpression::MathExpression(){
	ProcessInput();
}

MathExpression::MathExpression(string str){
	ProcessInput(str);
}

// Read expression string from console
void MathExpression::ProcessInput(){
    
#ifdef _WIN32
	system("cls");
#endif
    
#ifdef __APPLE__
    system("clear");
#endif

	cout << "Enter a math expression" << endl;
	cout << "Valid Characters: " << operandChars.substr(0,11) << operatorChars << endl;

	// cin needs to be empty or it won't wait for the user
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');

	ProcessInput(cin);
}

// Get input from a string
void MathExpression::ProcessInput(string str){
	//system("cls");

	stringstream ss;
	ss << str;
	cout << str << " = ";
	ProcessInput(ss);
}

void MathExpression::ProcessInput(istream & cin){
	char c = cin.peek();
	bool isPreviousCharOperand = false;	// if the first char is a hyphen, it has to be a minus sign
	bool isValidChar, isOperandChar, isOperatorChar;
	log.clear();

	Log("---------------------------\nStarted Parsing Input\n");

	while (c != '\n' && c != -1){
		if (cin.peek() != -1){
			c = cin.peek();
		}
		else{
			break;
		}

		// try to find the char in the "validCharacters" string
		isOperandChar = operandChars.find_first_of(c) != string::npos;
		isOperatorChar = operatorChars.find_first_of(c) != string::npos;
		isValidChar = isOperandChar ^ isOperatorChar; // must be one or the other, but not both! + and - are special cases

		if (isValidChar){
			isOperandChar ? ReadNumber(cin) : ReadOperator(cin);
			isPreviousCharOperand = isOperandChar;
		}
		else if (isalpha(c)){
			// function or variable
			ReadNamedObject(cin);
		}
		else if (c == '-' || c == '+'){
			// if the previous char is an operator, then character is a minus sign
			// if the previous char is an operand, then character is a subtraction operator

			isPreviousCharOperand ? ReadOperator(cin) : ReadNumber(cin);
			isPreviousCharOperand = !isPreviousCharOperand;
		}
		else{
			cin.ignore(); //ignore insignificant characters
		}
	}

	Move(); // move all tokens in the stack into the queue;
	Log("\nRPN: " + ToRPN());
	Log("\nFinished Parsing Input\n---------------------------");
}

void MathExpression::ReadNumber(istream & cin){
	stringstream ss;
	char c = cin.get();
	bool isIntegerType = c != '.';

	// The first value is can be a digit, a sign (+ or -), or a decimal point
    ss << c;

	if (c == '-' || c == '+'){ // whitespace between a minus sign and the first digit is acceptable as input
		c = cin.peek();

		// eliminate any whitespace
		while (c == ' '){
			cin.ignore();		
			c = cin.peek();
		}

		if (c == '\n'){
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			return; // no operand is added. This treats the value as -0 or +0
		}
	}

	c = cin.peek();

	while (operandChars.find_first_of(c) != string::npos && c != '-' && c != '+'){ // signs can only appears at the beginning of an operand
		if (c == '.'){
            if(isIntegerType){
                isIntegerType = false;
            }else{
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
                throw runtime_error("Parse Error: Multiple decimal points in operand");
            }
		}

		c = cin.get();	// read next char
		ss << c;		// push char to stringstream
		c = cin.peek(); // peek at next char
	}
    
	try{
		OperandToken t = (isIntegerType ? OperandToken(stoi(ss.str())) : OperandToken(stof(ss.str())));
		Log("Pushed to queue: " + t.ToString());
		output.push(t);
		AddRPN(ss.str());
	}
	catch (...){
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw runtime_error("Parse Error: Unable to process: " + ss.str());
	}
}

void MathExpression::ReadOperator(istream & ss){
	char c = static_cast<char>(ss.get());   // operators are only 1 character long
	OperatorToken token = OperatorToken(c);
	OperatorToken topToken = OperatorToken(Null);

	// operators do not come first, except for grouping operators. Note that the plus/minus signs are not evaluated in this method (They are part of the ReadNumber method)
	if (output.empty() && token.GetOperation() != Grouping){
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw runtime_error("Parse Error: Unexpected operator");
	}

	if (token.GetOperation() != Grouping){
		while (!opStack.empty()){
			topToken = opStack.top();

			// o1 is left-associative and is equal-precedence to o2, or o1 is less-precdence than o2
			// then ... pop o2 off stack, and onto output queue

            if(token < topToken) // overloaded operator!
			{
				Move(1);
				continue;
			}
			else{
				break;
			}
		}
	}
	else if(c == ')'){
		if (output.empty()){
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			throw runtime_error("Parse Error: Unexpected Parenthesis");
		}
		Log("Dropping token: " + token.ToString());
		MathToken t = opStack.top(); // can be function or operator

		// left parenthesis immediately goes to the stack and right parenthesis requires some moving items from the stack to the output queue
		// right parenthesis aren't pushed to the stack, so any grouping operator on the stack will be the left
		while (!opStack.empty()){
            
			// can be anything except a parenthesis
			if (t.category == Operator && t.Value.cValue == '('){
                break;
			}

			Move(1);
			t = opStack.top();
		}

		// If queue is not empty, the top element should be the left parenthesis
		if (!opStack.empty() && opStack.top().category == Operator && opStack.top().Value.cValue != '(' ){
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			throw runtime_error("Invalid Expression. Mismatched Parenthesises");
		}
		else{			
			Log("Dropping token: " + t.ToString());
			opStack.pop();	// left parenthesis doesn't get pushed to the queue
		}

		return; // don't push parenthesis to stack
	}

	opStack.push(token);
	Log("Pushed to stack: " + token.ToString());
}

void MathExpression::ReadNamedObject(istream & ss){
	// named object is either a math function or a math variable
	// None of these are required for the assignment, but I thought was pretty cool to make it this far

	char c = ss.peek();
	stringstream name;

	// get the name of the object. The name must be made of only letters
	while (ss.peek() != -1){
		c = ss.peek();

		if (isalpha(c)){
			ss.get(c);
			c = tolower(c);
			name << c;
		}
		else{
			break;
		}
	}

	// find the next operator or operand
	while (c == ' '){
		ss.ignore();
		c = ss.peek();
	}

	if (c == '('){
		FunctionToken fn = FunctionToken(name.str());
		opStack.push(fn);
		Log("Pushed to stack: " + fn.ToString());
	}
	else if (operatorChars.find_first_of(c) != string::npos || c == '\n'){
		// variable. not supported!
		Log("Found Variable: " + name.str());
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw runtime_error("Variables are not supported");
	}
	else{
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		throw range_error("Parse Error: Expecting Operator or Left Parenthesis");
	}
}

void MathExpression::Log(string message){
	if (useDebugMode){
		log.push_back(message);
	}
}

void MathExpression::Move(){
	Move(static_cast<int>(opStack.size()));
}

void MathExpression::Move(int count){
	MathToken token;

	// count can't be bigger than size
	if (count <= static_cast<int>(opStack.size())){
		while (!opStack.empty() && count > 0){
			token = opStack.top();

			if (token.category == Operator){
				if (static_cast<OperatorToken>(token).GetOperation() == TokenOperation::Grouping){
					throw runtime_error("Parse Error: Mismatched parenthesis");
				}

				AddRPN(token.Value.cValue);
				Log("Moved to queue: " + static_cast<OperatorToken>(token).ToString());
			}
			else if (token.category == Function){
				// get name of function

				FunctionToken func = static_cast<FunctionToken>(token);

				if (func.GetFunction() == Tangent){
					AddRPN("tan");
				}
				else if (func.GetFunction() == Sine){
					AddRPN("sin");
				}
				else if (func.GetFunction() == Cosine){
					AddRPN("cos");
				}
				else{
					AddRPN("func");
				}

				Log("Moved to queue: " + static_cast<FunctionToken>(token).ToString());
			}

			output.push(opStack.top());
			opStack.pop();
			count--;
		}
	}
	else{
		throw runtime_error("Parse Error: Can't move that many tokens");
	}
}

string MathExpression::getLog(){
	stringstream ss;

    for(size_t i = 0; i < log.size(); i++){
        ss << log.at(i) << endl;
    }

	return ss.str();
}

string MathExpression::ToRPN(){
	stringstream ss;

	for (vector<string>::size_type t = 0; t < RPN.size(); t++){
		ss << RPN[t] << " ";
	}

	return ss.str();
}

void MathExpression::AddRPN(int value){
	AddRPN(to_string(value));
}
void MathExpression::AddRPN(float value){
	AddRPN(to_string(value));
}
void MathExpression::AddRPN(char value){
	stringstream ss;
	ss << value;
	AddRPN(ss.str());
}
void MathExpression::AddRPN(string value){
	RPN.push_back(value);
}