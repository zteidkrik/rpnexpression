#include "stdafx.h"
#include "OperatorToken.h"

/* constructors */
OperatorToken::OperatorToken(char value)
	: MathToken(TokenCategory::Operator)
{
	if (value != 0){
		Value.cValue = value;
		type = TokenType::Character;

		SetOperation();
		SetAssociativity();
		SetPrecedence();
	}
}

OperatorToken::OperatorToken(const MathToken& token)
	: MathToken(TokenCategory::Operator)
{
	type = token.type;
	Value = token.Value;

	SetOperation(); // must come first
	SetAssociativity();
	SetPrecedence();
}

/* operators */

bool OperatorToken::operator<(OperatorToken &token){
    return GetPrecedence() < token.GetPrecedence() || (GetAssociativity() == TokenAssociativity::Left && GetPrecedence() == token.GetPrecedence());
}

bool OperatorToken::operator>(OperatorToken &token){
    return !operator<(token);
}

/* methods */

void OperatorToken::SetAssociativity(){
	// TokenOperation must be set!

	if (op == TokenOperation::Addition || op == TokenOperation::Subtraction || op == TokenOperation::Multiplication || op == TokenOperation::Division){
		associativity = TokenAssociativity::Left;
	}
	else if (op == TokenOperation::Power){
		associativity = TokenAssociativity::Right;
	}
	else if (op == TokenOperation::NoOperation){
		associativity = TokenAssociativity::NoAssociativity;
	}
}

void OperatorToken::SetPrecedence(){
	// Only applies to operators
	if (op == TokenOperation::Addition || op == TokenOperation::Subtraction){
		precedence = TokenPrecedence::AdditionSubtraction;
	}
	else if (op == TokenOperation::Multiplication || op == TokenOperation::Division || op == TokenOperation::Modulus){
		precedence = TokenPrecedence::MultiplicationDivisionModulo;
	}
	else if (op == TokenOperation::Power){
		precedence = TokenPrecedence::ExponentsRoots;
	}
    else if (op == TokenOperation::Grouping){
        precedence = TokenPrecedence::GroupingScope;
    }
	else{
		precedence = TokenPrecedence::NoPrecedence;
	}
}

void OperatorToken::SetOperation(){
	char value = Value.cValue;

	arity = TokenArity::Binary;

	// Set the operation flag
	if (value == '+'){
		op = Addition;
	}
	else if (value == '-'){
		op = Subtraction;
	}
	else if (value == '*'){
		op = Multiplication;
	}
	else  if (value == '/'){
		op = Division;
	}
	else if (value == '(' || value == ')'){
		op = Grouping;
		arity = Nullary;
	}
	else if (value == '%'){
		op = TokenOperation::Modulus;
	}
	else if (value == '^'){
		op = TokenOperation::Power;
	}
	else if (value == '!'){
		op = TokenOperation::Factorial;
		arity = Unary;
	}
	else{
		throw runtime_error("Invalid Operator: " + value);
	}
}

TokenOperation OperatorToken::GetOperation(){
	return op;
}

TokenAssociativity OperatorToken::GetAssociativity(){
	return associativity;
}

TokenPrecedence OperatorToken::GetPrecedence(){
	return precedence;
}

TokenArity OperatorToken::GetArity(){
	return arity;
}

std::string OperatorToken::TypeName(){
	if (op == Addition){
		return "Addition";
	}
	else if (op == Subtraction){
		return "Subtraction";
	}
	else if (op == Multiplication){
		return "Multiplication";
	}
	else if (op == Division){
		return "Division";
	}
	else if (op == Modulus){
		return "Modulus";
	}
	else if (op == Power){
		return "Power";
	}
	else if (op == Factorial){
		return "Factorial";
	}
	else{
		return "Operation";
	}
}