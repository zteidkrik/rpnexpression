#include "stdafx.h"
#include "FunctionToken.h"



FunctionToken::FunctionToken(string name)
	: MathToken(Function)
{
	type = Character;
	Value.cValue = 'f';

	_name = name;

	if (_name == "tan"){
		function = Tangent;
		Value.cValue = 't';
	}
	else if (_name == "cos"){
		function = Cosine;
		Value.cValue = 'c';
	}
	else if (_name == "sin"){
		function = Sine;
		Value.cValue = 's';
	}
	else{
		throw runtime_error("Unknown function name");
	}
}

FunctionToken::FunctionToken(const MathToken& token)
	: MathToken(Function)
{
	type = token.type;
	Value = token.Value;

	SetFunction();
}

TokenFunction FunctionToken::GetFunction(){
	return function;
}

TokenArity FunctionToken::GetArity(){
	return arity;
}

void FunctionToken::SetFunction(){
	char c = Value.cValue;

	arity = Unary; // only unary functions are used

	if (c == 't'){
		function = Tangent;
		_name = "tan";
	}
	else if (c == 's'){
		function = Sine;
		_name = "sin";
	}
	else if (c == 'c'){
		function = Cosine;
		_name = "cos";
	}
}

MathToken FunctionToken::operator<<(const MathToken& value){
    OperandToken token = OperandToken(Float);
    token.Value = value.Value;
    
	float v = (value.type == Float ? value.Value.fValue : value.Value.iValue) * PI / 180.0f;
        
	if (GetFunction() == Tangent){
		v = tan(v);
	}
	else if (GetFunction() == Cosine){
		v = cos(v);
	}
	else if(GetFunction() == Sine){
		v = sin(v);
	}else{
        throw runtime_error("Unsupported function");
    }
    
    token.Value.fValue = v;
    
	return token;
}

string FunctionToken::ToString(){
	return MathToken::ToString() + ":" + _name;
}